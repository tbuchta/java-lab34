/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class containing the Post entity
 * @version 1.1
 * @author Tomasz Buchta
 */
@Entity
@Table(name = "POST")
@NamedQueries({
    @NamedQuery(name = "Post.findAll", query = "SELECT p FROM Post p"),
    @NamedQuery(name = "Post.findById", query = "SELECT p FROM Post p WHERE p.id = :id"),
    @NamedQuery(name = "Post.findByBody", query = "SELECT p FROM Post p WHERE p.body = :body"),
    @NamedQuery(name = "Post.findByTitle", query = "SELECT p FROM Post p WHERE p.title = :title")})
public class Post implements Serializable {
     /**
     * Field holding serial version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Field holding Post Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    /**
     * Field holding Post body
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "BODY")
    private String body;
     /**
     * Field holding Post title
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "TITLE")
    private String title;
    @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false,cascade = CascadeType.REMOVE)
    private Author authorId;

     /**
     * Parameterless constructor
     */
    public Post() {
    }

     /**
     * Constructor
     * @param id to be assign to Post
     */
    public Post(Integer id) {
        this.id = id;
    }

    /**
     * Constructor
     * @param id to be assigned
     * @param body to be assigned
     * @param title to be assigned
     */
    public Post(Integer id, String body, String title) {
        this.id = id;
        this.body = body;
        this.title = title;
    }

    /**
     * Returns the Post id
     * @return post id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the post Id
     * @param id to be set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Returns the body of Post
     * @return the string containing body of Post
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the body field
     * @param body to be set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * Returns the title of Post
     * @return string containing title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the Post title
     * @param title to be set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the Post id
     * @return Post id
     */
    public Author getAuthorId() {
        return authorId;
    }

    /**
     * Sets the Author id for association
     * @param authorId of the author to be associated
     */
    public void setAuthorId(Author authorId) {
        this.authorId = authorId;
    }

    /**
     * Returns the hash code of post
     * @return hash code of object
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

     /**
     * Post comparison
     * @param object for comparison
     * @return true if objects are the same else returns false
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Post)) {
            return false;
        }
        Post other = (Post) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

     /**
     * Method used for representing entity in string
     * @return string representing the Post entity
     */
    @Override
    public String toString() {
        return "pl.polsl.buchta.tomasz.entities.Post[ id=" + id + " ]";
    }
    
}
