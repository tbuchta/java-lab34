/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @version 1.2
 * @author Tomasz Buchta
 * Class containing the Author entity
 */
@Entity
@Table(name = "AUTHOR")
@NamedQueries({
    @NamedQuery(name = "Author.findAll", query = "SELECT a FROM Author a"),
    @NamedQuery(name = "Author.findById", query = "SELECT a FROM Author a WHERE a.id = :id"),
    @NamedQuery(name = "Author.findByFirstname", query = "SELECT a FROM Author a WHERE a.firstname = :firstname"),
    @NamedQuery(name = "Author.findByLastname", query = "SELECT a FROM Author a WHERE a.lastname = :lastname")})
public class Author implements Serializable {
    /**
     * Field holding serial version UID
     */
    private static final long serialVersionUID = 1L;
     /**
     * Field holding Author Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    /**
     * Field holding Author firstname
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "LASTNAME")
    private String lastname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "authorId",orphanRemoval = true)
    private List<Post> postList;

    /**
     * Parameterless constructor
     */
    public Author() {
    }

    /**
     * Constructor
     * @param id to be assign to Author
     */
    public Author(Integer id) {
        this.id = id;
    }

    /**
     * Constructor
     * @param id to be assigned
     * @param firstname to be assigned
     * @param lastname to be assigned
     */
    public Author(Integer id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    /**
     * Return author Id
     * @return the Author id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets author id
     * @param id to be set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return Authors firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets author firstname
     * @param firstname to be set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Return Author lastname
     * @return the Author lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the author lastname
     * @param lastname to be set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Returns lists of posts associated to author
     * @return List of posts associated to author
     */
    public List<Post> getPostList() {
        return postList;
    }

    /**
     * Sets the associated posts list
     * @param postList list of posts to associated with Author
     */
    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    /**
     * Returns the hash code of author
     * @return hash code of object
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     * Author comparison
     * @param object for comparison
     * @return true if objects are the same else returns false
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Author)) {
            return false;
        }
        Author other = (Author) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     * Method used for representing entity in string
     * @return string representing the Author entity
     */
    @Override
    public String toString() {
        return "pl.polsl.buchta.tomasz.entities.Author[ id=" + id + " ]";
    }
    
}
