/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.model;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.polsl.buchta.tomasz.entities.Author;

/**
 *
 * @version 1.0
 * @author Tomasz Buchta
 * Facade implementation for Author entity
 */
@Stateless
public class AuthorFacade extends AbstractFacade<Author> {
    /**
     * EntityManager instance
     */
    @PersistenceContext(unitName = "Lab34-EJB")
    private EntityManager em;

    /**
     * returns EntityManager instance
     * @return instance of EntityManager
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Parameterless constructor
     */
    public AuthorFacade() {
        super(Author.class);
    }
    
}
