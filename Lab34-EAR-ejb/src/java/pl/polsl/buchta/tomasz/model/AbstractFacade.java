/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.model;

import java.util.List;
import javax.persistence.EntityManager;

/**
 * @version 1.0
 * @author Tomasz Buchta
 * Abstract class used for Facade pattern
 */
public abstract class AbstractFacade<T> {
    /**
     * Field holding the Entity class
     */
    private Class<T> entityClass;

    /**
     * Constructor
     * @param entityClass to be used with Facade
     */
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Returns EntityManger instance of Facade
     * @return Facade EntityManager instance
     */
    protected abstract EntityManager getEntityManager();

    /**
     * Creates new Entity record in db
     * @param entity to be saved
     */
    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    /**
     * Edits the Entity record in db
     * @param entity to be edited
     */
    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    /**
     * Deletes the entity record from db
     * @param entity to be deleted from db
     */
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Finds the entity with requested id in db
     * @param id of entity to be found
     * @return entity found
     */
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    /**
     * Finds all entities
     * @return List of all entities stored in db
     */
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     * Finds range of entities
     * @param range 2 element array with lower and upper bound of entities to be found
     * @return List of entities in given bound
     */
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Counts the entities stored in db
     * @return number of Entity stored in db
     */
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
