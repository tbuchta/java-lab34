<%-- 
    Document   : error
    Created on : Dec 16, 2015, 5:56:18 PM
    Author     : tbuchta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <h1>There was an error</h1>
        
        Your request did not complete
        
        Some info about it:
        
        ${exceptionMessage}
        
        <a href="${originUrl}">Go back</a>
    </body>
</html>
