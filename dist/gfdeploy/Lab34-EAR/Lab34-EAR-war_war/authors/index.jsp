<%-- 
    Document   : index
    Created on : Dec 12, 2015, 6:34:21 PM
    Author     : tbuchta
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Authors Index</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <c:forEach items="#{authors}" var="author">
                <tr>
                    <td>${author.id}</td>
                    <td>${author.firstname}</td>
                    <td>${author.lastname}</td>
                    <td><a href="<c:url value="/AuthorEdit?id=${author.getId()}"/>">Edit</a></td>
                    <td><a href="<c:url value="/AuthorDestroy?id=${author.getId()}"/>">Destroy</a></td>
                </tr>
            </c:forEach>
        </table>
        <a href="AuthorNew">New Author</a>
        <a href="PostIndex">Posts</a>
    </body>
</html>
