<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form action="PostCreate" method="POST">
    Title<input type="text" name="title" value=${post.getTitle()} /></br>
    Body<input type="text" name="body" value=${post.getBody()} /></br>
    Author<select name="authorId">
        <c:forEach var="author" items="${authors}">
            <option value="${author.getId()}">${author.getFirstname()} ${author.getLastname()}</option>
        </c:forEach>
    </select>
    <input type="hidden" name="id" value=${post.getId()} />
    <input type="submit" value="Submit" />
</form>
