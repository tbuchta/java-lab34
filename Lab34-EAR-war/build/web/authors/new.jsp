<%-- 
    Document   : new
    Created on : Dec 12, 2015, 8:46:55 PM
    Author     : tbuchta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Author</title>
    </head>
    <body>
        <h1>New Author</h1>
        <jsp:include page="authorForm.jsp"></jsp:include>
    </body>
</html>
