/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.controller.author;

import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.buchta.tomasz.entities.Author;
import pl.polsl.buchta.tomasz.model.AuthorFacade;

/**
 * Servlet controlling the creation and edit of authors
 * @version 1.0
 * @author Tomasz Buchta
 */
public class AuthorCreate extends HttpServlet {
    /**
     * instance of authorFacade
     */
    @EJB
    private AuthorFacade authorFacade;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods. Handles creation and edit of author entities. If id param is present the servlet will try to updated the entity with given id
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String idParameter = request.getParameter("id");
        Author author;
        if(idParameter == null || idParameter.isEmpty()){
            author = new Author();
        }
        else{
            try{
                int id = Integer.parseInt(idParameter);
                author = authorFacade.find(id);
            }
            catch(NumberFormatException e){
                //There is no valid id
                author = new Author();
            }
        }
        try{
            author.setFirstname((String)request.getParameter("firstname"));
            author.setLastname((String)request.getParameter("lastname"));
            if(author.getId() == null){
                authorFacade.create(author);
            }
            else{
                authorFacade.edit(author);
            }
        }
        catch(EJBException e){
            String exceptionMessage = e.getCause().getMessage();
            request.setAttribute("exceptionMessage", exceptionMessage);
            String originUrl;
            if(author.getId() == null)
                originUrl = "AuthorNew";
            else
                originUrl = "AuthorEdit" + "?id=" + author.getId().toString();
            request.setAttribute("originUrl", originUrl);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        request.getRequestDispatcher("AuthorIndex").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
