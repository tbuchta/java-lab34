/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.controller.post;

import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.buchta.tomasz.entities.Post;
import pl.polsl.buchta.tomasz.model.AuthorFacade;
import pl.polsl.buchta.tomasz.model.PostFacade;

/**
 * Servlet controlling the post creation and update
 * @version 1.0
 * @author Tomasz Buchta
 * 
 * Servlet controlling the creation and edit of posts
 */
public class PostCreate extends HttpServlet {
    /**
     * Field holding the PostFacade instance
     */
    @EJB
    private PostFacade postFacade;
    /**
     * Field containing the instance of AuthorFacade
     */
    @EJB
    private AuthorFacade authorFacade;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods. Handles the creation and edit of post entity
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String idParameter = request.getParameter("id");
        Post post;
        if(idParameter == null || idParameter.isEmpty()){
            post = new Post();
        }
        else{
            try{
                int id = Integer.parseInt(idParameter);
                post = postFacade.find(id);
            }
            catch(NumberFormatException e){
                //There is no valid id
                post = new Post();
            }
        }
        try{
            post.setTitle((String)request.getParameter("title"));
            post.setBody((String)request.getParameter("body"));
            post.setAuthorId(authorFacade.find(Integer.parseInt(request.getParameter("authorId"))));
            if(post.getId() == null){
                postFacade.create(post);
            }
            else{
                postFacade.edit(post);
            }
        }
        catch(EJBException e){
            String exceptionMessage = e.getCause().getMessage();
            request.setAttribute("exceptionMessage", exceptionMessage);
            String originUrl;
            if(post.getId() == null)
                originUrl = "PostNew";
            else
                originUrl = "PostEdit" + "?id=" + post.getId().toString();
            request.setAttribute("originUrl", originUrl);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        catch (NumberFormatException e){
            String exceptionMessage = e.getMessage();
            request.setAttribute("exceptionMessage", exceptionMessage + "\n AuthorId cannot be empty");
            String originUrl;
            if(post.getId() == null)
                originUrl = "PostNew";
            else
                originUrl = "PostEdit" + "?id=" + post.getId().toString();
            request.setAttribute("originUrl", originUrl);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        request.getRequestDispatcher("PostIndex").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
