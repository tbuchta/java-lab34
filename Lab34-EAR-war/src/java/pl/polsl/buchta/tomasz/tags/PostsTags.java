/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.tags;

import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import pl.polsl.buchta.tomasz.entities.Post;

/**
 * Handler for custom tag
 * @version 1.0
 * @author Tomasz Buchta
 * 
 */
public class PostsTags extends SimpleTagSupport {
    /**
     * Field holding instance of collection passed to tag
     */
    private List<Post> collection;

    /**
     * Called by the container to invoke this tag. Renders table with following contents from appropriate posts in separate columns:
     * Id,title,body,Edit link,Destroy link
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            out.println("<table>");
            for(Post p : collection){
                out.println("<tr>");
                out.print("<td>" + p.getId() + "</td>");
                out.print("<td>" + p.getTitle() + "</td>");
                out.print("<td>" + p.getBody() + "</td>");
                out.print("<td>" + 
                        "<a href=PostEdit?id="
                        + p.getId() 
                        + "> Edit" 
                        + "</a>" 
                        + "</td>");
                out.print("<td>" + 
                        "<a href=PostDestroy?id="
                        + p.getId() 
                        + "> Destroy" 
                        + "</a>" 
                        + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            
            JspFragment f = getJspBody();
            if (f != null) {
                f.invoke(out);
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in Posts tag", ex);
        }
    }

    /**
     * Sets the collection to be used by tag
     * @param collection collection for tag to use
     */
    public void setCollection(List collection) {
        this.collection = collection;
    }
    
}
