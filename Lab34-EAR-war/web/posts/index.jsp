<%-- 
    Document   : index
    Created on : Dec 12, 2015, 6:34:21 PM
    Author     : tbuchta
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="m" uri="../WEB-INF/tlds/lib.tld" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Index</title>
    </head>
    <body>
        <h1>Listing Posts</h1>
        <table>
            <m:eachPost collection="${posts}">
                
            </m:eachPost>
        </table>
        <a href="PostNew">New Post</a>
        <a href="AuthorIndex">Authors</a>
    </body>
</html>
